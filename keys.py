import os

from libqtile.config import Key, Drag, Click
from libqtile.command import lazy

from groups import groups

# * Common variables
home = os.path.expanduser("~")
local_script = home + "/bin/"

mod = "mod4"
myTerm = "alacritty"
myPlayer = "ncmpcpp"
myInfo = "htop"
myLauncher = "dmenu_run -h 35"
myRofi = "rofi -combi-modi 'window,drun' -modi 'combi' -show combi"
myLocker = local_script + "i3lock-laptop.sh"

# * Keys
keys = [
    # ** Switch between windows in current stack pane
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "l", lazy.layout.right()),
    # ** Move windows up or down in current stack
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    # ** Grow windows up or down in current stack
    Key([mod, "mod1"], "h", lazy.layout.grow_left()),
    Key([mod, "mod1"], "j", lazy.layout.grow_down()),
    Key([mod, "mod1"], "k", lazy.layout.grow_up()),
    Key([mod, "mod1"], "l", lazy.layout.grow_right()),
    # ** Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),
    # ** Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),
    # ** Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    # ** Applications
    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "m", lazy.spawn(myTerm + " --class MPCWindow -e " + myPlayer)),
    Key([mod], "i", lazy.spawn(myTerm + " --class HTOPWindow -e " + myInfo)),
    Key([mod], "r", lazy.spawn(myLauncher)),
    Key([mod], "p", lazy.spawn(myRofi)),
    # ** Lock laptop
    Key([mod, "control"], "l", lazy.spawn(myLocker)),
    # ** Toggle between different layouts
    Key([mod], "Tab", lazy.next_layout()),
    # ** Window/Session control
    Key([mod, "shift"], "c", lazy.window.kill()),
    Key([mod, "control"], "r", lazy.restart()),
    Key(
        [mod, "shift"],
        "q",
        lazy.spawn(
            local_script
            + "dmenu/prompt "
            + '"Really quit QTile ? This will close X server ! " '
            + '"qtile-cmd -o cmd -f shutdown"'
        ),
    ),
    # ** Media keys
    Key([], "XF86AudioPlay", lazy.spawn("mpc -q toggle")),
    Key([], "XF86AudioPause", lazy.spawn("mpc -q pause")),
    Key([], "XF86AudioStop", lazy.spawn("mpc -q stop")),
    Key([], "XF86AudioPrev", lazy.spawn("mpc -q prev")),
    Key([], "XF86AudioNext", lazy.spawn("mpc -q next")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q -D pulse sset Master 4%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q -D pulse sset Master 4%+")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -q -D pulse sset Master toggle")),
    Key(
        [],
        "XF86MonBrightnessUp",
        lazy.spawn("brightnessctl -d intel_backlight set 4%+"),
    ),
    Key(
        [],
        "XF86MonBrightnessDown",
        lazy.spawn("brightnessctl -d intel_backlight set 4%-"),
    ),
]

# * Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

# * Assign workspace switching
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key([mod], i.name, lazy.group[i.name].toscreen()),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        ]
    )
