import os
import subprocess

from libqtile import layout, hook

from typing import List
from keys import keys
from groups import groups
from screens import screens
from bars import widget_defaults


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])


layouts = [
    layout.Bsp(border_normal="#0C0C14",
               border_focus="#2461B7",
               border_width=3),
    layout.Max(),
    layout.Stack(num_stacks=2,
                 border_normal="#0C0C14",
                 border_focus="#2461B7",
                 border_width=3),
    layout.MonadTall(border_normal="#0C0C14",
                     border_focus="#2461B7",
                     border_width=3),
    layout.Tile(border_normal="#0C0C14",
                border_focus="#2461B7",
                border_width=3),
]

extension_defaults = widget_defaults.copy()

dgroups_key_binder = None
dgroups_app_rules: List = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'ksshaskpass'},  # ksshaskpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

wmname = "LG3D"
